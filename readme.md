Perquisite:

Graphic driver
OpenCL driver

apt install cmake build-essential git libglfw3-dev libassimp-dev libglm-dev
git submodule foreach git pull 

# Overview

The library provides a window class based on glfw3. When you create a Window variable it does all
the basic initialization for you. If you need any special features that are nor provided
by the interface you can use the glfw-window-pointer directly.

```
GLq::Window win;
win.camera.Position += vec3(10.0f, 2.0f, 35.0);
// Using the window-pointer to override default behaivor
glfwSetInputMode(win.getWindowPointer(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
```

Next we need to set up the shader. Currently only vertex,fragment and geometry shader are supported.

```
Shader shader("path/to/vertex","path/to/fragment");
```

The basic set up is already done. Next we need to create some geometry to be displayed and
some lights so we can see your geometry. In order to do so we first load a obj model from a file.
Next we create a light, we can choose to attach a geometry to the light. The library provides a few
basic geometric shapes like cube, sphere or plane (we are using a pointer here so that we don't have to
provide a geoemtry). The lightcollection makes sure the lightinformation are passed to a Uniform
Buffer Object so that the data is available in all shader at render time.

```
Model m("path/to/obj");
Cube cube(vec3())
Light l(vec3(),&cube);
LighCollection lc;
lc.attachLight(l);
```

Now we can start the render loop.

```
 while (!win.shoudClose()) {
        glClearColor(0.4f, 0.4f, 0.4f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        win.process_inputs();
        lc.updateUniBuffer();
        lc.draw(light_shader);
        
        // TODO move into window class
        glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float) win.width / (float) win.height, 0.1f,
                                                        100000.0f);
        win.camera.updateUniBuffer(projection);
        
        m.draw(shader);
        win.update();        
```

# Drawable Meshes and Geometry

In order to draw geometry we provide the GPU with a list of vertices. Each vertex carries additional
information such as normal vector which depends on the currently processed face. As such if 
a point contributes to multiple faces multiple vertices for that point will be created. As such
this representation is not usable for geometry manipulation. Instead a separate `GeometryMesh` class
should be created which can convert itself to `RenderMesh` when needed(I would recommend to make this 
explicit as it is a rather expensive conversion). 

# TODO

 * TODO bring Projection into camera (glq)
 * have one window update that processes Keys and camera instead of two function calls.