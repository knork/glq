//
// Created by knork on 2/23/18.
//

#ifndef WOODFRACTURE_LIGHT_H
#define WOODFRACTURE_LIGHT_H


#include <glm/vec3.hpp>
#include <glm/detail/type_float.hpp>
#include <glad/glad.h>
#include <vector>
#include "src/drawable/Drawable.h"



class Light{
    Drawable* attached;
    glm::vec3 position;

public:
    explicit Light(glm::vec3 pos,Drawable* attached = nullptr):position(pos),attached(attached){}

    const glm::vec3 &getPosition() const {
        return position;
    }

    void setPosition(const glm::vec3 &position) {
        Light::position = position;
    }

    Drawable *getAttached() const {
        return attached;
    }

    void setAttached(Drawable *attached) {
        Light::attached = attached;
    }
};

/**
 * Collects all the lights and their information in the system.
 * Sends them as one big uniform buffer data-stream to the
 * gpu making them availible in all Shaders.
 *
 * GL_UNIFORM_BUFFER index 0
 */
class LightCollection{

    // struct with the same padding as glsl std140 layout
    struct GLSL_Lights {
        glm::vec3 ambient; // 12 Byte
        glm::float32 padding; // 4 Bytes
        // 16
        glm::vec3 pos;// 12 Bytes
        glm::float32 padding2; // 4 Bytes
        // 32
        glm::uint type; // 4 Bytes
        glm::float32 spec; // 4 Bytes
        glm::float32 padding3; // 4 Bytes
        glm::float32 padding4; // 4 Bytes
        // Total : 48 Bytes
        GLSL_Lights(glm::vec3 position):ambient(glm::vec3(0)),pos(position),type(0),spec(0){}
    };

    // working with max of 30 lights and an additional int to pass the actual number of lights
    // max for intelGPU is 16KB thus max radius of lights is ~ 340
    const GLuint lightBufferSize = 30 * 48 + 16;
    const GLuint bufferIndex = 1;

    GLuint uboMatrices;
    mutable std::vector<GLSL_Lights> curLightStructs;
    std::vector<Light> allLights;
    void initBuffer();




public:
    LightCollection();
    /// Lights need to be attached to discrete Scene Object
    /// which has a position assiciated with it
    void attachLight(Light light);
    /// call before draw -> update lights information in buffer
    void updateUniBuffer()const;

    void draw(Shader& shader)const;
};

#endif //WOODFRACTURE_LIGHT_H
