//
// Created by knork on 12/11/17.
//

#include "../extern/tinyobject.h"
#include <glm/glm.hpp>
#include "geometryModel.h"
#include "model.h"


GeometryModel::GeometryModel(std::string path) {

	tinyobj::attrib_t attributes;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;
	string directory = path.substr(0, path.find_last_of('/'));
	tinyobj::LoadObj(&attributes, &shapes, &materials, &err, path.c_str(), directory.c_str());


	size_t cur = 0;
	size_t nFloat = attributes.vertices.size();
	nFloat = nFloat / 3;
	vector<tinyobj::real_t> &verts = attributes.vertices;

	for (unsigned int i = 0; i < nFloat; i++) {
		cur = i * 3;



		vertices.push_back(
				glm::vec3(attributes.vertices[cur],
						  attributes.vertices[cur + 1],
						  attributes.vertices[cur + 2]));
	}




	nFloat = attributes.normals.size();
	nFloat = nFloat / 3 ;
	tinyobj::real_t *norm = &attributes.normals[0];
	for (unsigned int i = 0; i < nFloat; i++) {
		cur = i * 3;
		normals.push_back(glm::vec3(norm[cur], norm[cur + 1], norm[cur + 2]));
	}


	nFloat = attributes.texcoords.size();
	nFloat = nFloat / 2 ;
	tinyobj::real_t *tex = &attributes.texcoords[0];
	for (unsigned int i = 0; i < nFloat; i++) {
		cur = i * 2;
		texCoords.push_back(glm::vec2(tex[cur], tex[cur + 1]));
	}


	//currently only supporting a single shape
	assert(shapes.size() == 1);

	vertex_indices = shapes[0].mesh.indices;


}


void GeometryModel::createRenderableMesh() {
	//convert data
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	//assert(typeid(tinyobj::real_t) == typeid(float));
	vertices.reserve(vertex_indices.size());

	vector<glm::vec3> &v = this->vertices;
	vector<glm::vec3> &n = this->normals;
	vector<glm::vec2> &t = this->texCoords;

	vector<tinyobj::index_t> &I = vertex_indices;

	Vertex c;


	int i3 = 0, i2 = 0;
	assert(I.size() % 3 == 0);

	for (int i = 0; i < I.size(); i++) {
		i3 = I[i].vertex_index;
		c.Position = v[i3];
		i3 = I[i].normal_index;
		c.Normal = normals[i3];

		i3 = I[i].texcoord_index;

		c.TexCoords = t[i3];

		vertices.push_back(c);
		indices.push_back(indices.size());
	}



	vector<Texture> tex;
	mesh.clear();
	mesh.push_back({vertices, indices, tex});

}

void GeometryModel::draw(Shader &shader) const {
	for (auto &m:mesh) {
		m.draw(shader);
	}
}

void GeometryModel::draw(Shader &shader,glm::mat4 trans) const {
	shader.setMat4("model",trans);
	for (auto &m:mesh) {
		m.draw(shader);
	}
}

const vector<RenderMesh> &GeometryModel::getMesh() const {
	return mesh;
}

void GeometryModel::setMesh(const vector<RenderMesh> &mesh) {
	GeometryModel::mesh = mesh;
}

const vector<tinyobj::index_t> &GeometryModel::getVertex_indices() const {
	return vertex_indices;
}

void GeometryModel::setVertex_indices(const vector<tinyobj::index_t> &vertex_indices) {
	GeometryModel::vertex_indices = vertex_indices;
}

const vector<glm::vec3> &GeometryModel::getVertices() const {
	return vertices;
}

void GeometryModel::setVertices(const vector<glm::vec3> &vertices) {
	GeometryModel::vertices = vertices;
}

const vector<glm::vec3> &GeometryModel::getNormals() const {
	return normals;
}

void GeometryModel::setNormals(const vector<glm::vec3> &normals) {
	GeometryModel::normals = normals;
}

const vector<glm::vec2> &GeometryModel::getTexCoords() const {
	return texCoords;
}

void GeometryModel::setTexCoords(const vector<glm::vec2> &texCoords) {
	GeometryModel::texCoords = texCoords;
}

vector<glm::vec3> &GeometryModel::getVertices() {
	return vertices;
}

vector<glm::vec3> floatToVec3(float *vertices, unsigned int length) {
	assert(length%3 ==0);
	size_t cur=0;
	vector<glm::vec3> out;
	out.reserve(length/3);
	for (unsigned int i = 0; i < length/3; i++) {
		cur = i * 3;
		out.push_back(glm::vec3(vertices[cur], vertices[cur + 1], vertices[cur + 2]));
	}

	return out;
}
