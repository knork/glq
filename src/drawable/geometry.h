//
// Created by knork on 6/21/17.
//

#ifndef FLUIDSIM_GEOMETRY_H
#define FLUIDSIM_GEOMETRY_H
#include <glm/glm.hpp>

#include <vector>
#include <glad/glad.h>
#include "../shader.h"
#include "mesh.h"
#include "Drawable.h"

/**
 * Represents an object in the scene that is only
 * drawable but can not interact with other objects.
 * However they can be attached PhysicsObjects to
 * give the PhysicObject a visual representation.
 */

class DrawableObj: public Drawable {
protected:
	GLuint VAO, VBO, EBO;

	glm::vec3 position;
	std::vector<GLuint> indices;
	std::vector<Vertex> vertices; // in obj-space

	static std::vector<Shader *> shader;
	static std::vector<std::vector<glm::mat4>> model;


	/**
	* Expects vertices-array to be filled.
	* If indices array is filled it will also be pushed to GPU
	*/
	virtual void initBuffers();

public:
	glm::vec4 color;
	DrawableObj(glm::vec3 pos) : position(pos){}
	virtual ~ DrawableObj();


	virtual void draw(Shader &shader)const {
		draw(shader, glm::mat4());
	}
	virtual void draw(Shader &shader, glm::mat4 model) const override =0  ;

};




class Sphere : public DrawableObj {
public:
//    Sphere(glm::vec3 pos, glm::vec3 vel):RigidBody(pos,vel){}
	Sphere(glm::vec3 pos, int res);
	~Sphere(){};

	void draw(Shader &shader) const override;
	void draw(Shader &shader, glm::mat4 model) const override;
protected:
//    void initBuffers() override; GL_LINES
private:
	void initSolidSphere(int resolution);
	void refineSolidSphere(const std::vector<glm::vec3>& sphere,std::vector<glm::vec3>& refined);
};

class Cube : public DrawableObj{
public:
	Cube(glm::vec3 pos);
	~Cube() override;

	void draw(Shader &shader) const override;
	virtual void draw(Shader &shader, glm::mat4 model) const override;

protected:
	void initBuffers() override;
};


class Plane: public DrawableObj{
public:
	Plane(glm::vec3 pos, float extend);

	void draw(Shader &shader) const override;

	void draw(Shader &shader, glm::mat4 model) const override;
};




#endif //FLUIDSIM_GEOMETRY_H
