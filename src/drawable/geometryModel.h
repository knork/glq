//
// Created by knork on 12/11/17.
//

#ifndef OPENCL_SMOOTH_GEOMETRYMODEL_H
#define OPENCL_SMOOTH_GEOMETRYMODEL_H


#include "../extern/tinyobject.h"
#include "../shader.h"
#include "mesh.h"

using namespace std;

/**
 * Only stores vertices once and actually refrences them through indices
 * however before we can draw we have to expand and "flatten" the array
 */
class GeometryModel {
public:
	GeometryModel(std::string path);


	void createRenderableMesh();
	void draw(Shader& shader)const;
	void draw(Shader &shader,glm::mat4 trans)const;

	const vector<RenderMesh> &getMesh() const;

	void setMesh(const vector<RenderMesh> &mesh);

	const vector<tinyobj::index_t> &getVertex_indices() const;

	void setVertex_indices(const vector<tinyobj::index_t> &vertex_indices);

	const vector<glm::vec3> &getVertices() const;

	 vector<glm::vec3> &getVertices();

	void setVertices(const vector<glm::vec3> &vertices);

	const vector<glm::vec3> &getNormals() const;

	void setNormals(const vector<glm::vec3> &normals);

	const vector<glm::vec2> &getTexCoords() const;

	void setTexCoords(const vector<glm::vec2> &texCoords);


private:
	vector<RenderMesh> mesh;
	vector<tinyobj::index_t> vertex_indices;


	vector<glm::vec3> vertices;
	vector<glm::vec3> normals;
	vector<glm::vec2> texCoords;





};

vector<glm::vec3> floatToVec3(float *vertices, unsigned int length);


#endif //OPENCL_SMOOTH_GEOMETRYMODEL_H
