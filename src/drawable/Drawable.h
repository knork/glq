//
// Created by knork on 12/5/17.
//

#ifndef OPENGL_QUICKSTART_PRIMITIVE_H
#define OPENGL_QUICKSTART_PRIMITIVE_H

#include "../shader.h"

class Drawable{
public:
	virtual void draw(Shader& in) const = 0;
	virtual void draw(Shader& in, glm::mat4 transform) const = 0;
};

#endif //OPENGL_QUICKSTART_PRIMITIVE_H
