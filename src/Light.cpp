//
// Created by knork on 2/23/18.
//

#include "Light.h"
void LightCollection::attachLight(Light l) {
    allLights.push_back(l);
}


void LightCollection::initBuffer() {
    glGenBuffers(1, &uboMatrices);
    glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
    glBufferData(GL_UNIFORM_BUFFER, lightBufferSize, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, bufferIndex, uboMatrices, 0, lightBufferSize * sizeof(GLbyte));
}

void LightCollection::updateUniBuffer() const {

    curLightStructs.clear();
    for(auto & in:allLights) {
        curLightStructs.push_back(GLSL_Lights(in.getPosition()));
    }

    glm::uint amount = allLights.size();
    if (amount >= 30)
        amount = 30;

    glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
    // offset is 16 ... why?!? dunno
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::uint), &(amount));
    glBufferSubData(GL_UNIFORM_BUFFER, 16, amount * sizeof(GLSL_Lights), &curLightStructs[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

}

void LightCollection::draw(Shader &shader) const {

    for(auto& d: allLights){
        glm::mat4 p;
        p=glm::translate(p,d.getPosition());
        d.getAttached()->draw(shader,p);
    }

}

LightCollection::LightCollection() {
    initBuffer();
}
