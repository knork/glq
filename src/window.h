//
// Created by knork on 12/5/17.
//

#ifndef OPENGL_QUICKSTART_WINDOW_H
#define OPENGL_QUICKSTART_WINDOW_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "camera.h"

namespace GLq {

	class Window {
		GLFWwindow *window;
		/// Used to generate dt
		float deltaTime, lastFrame;
		bool firstMouse = true;
		double lastX, lastY;

		bool paused = false;
		bool slowMo = false;
		const float normal = 1.8, slow = 9.8f;
		float curTimeCo = 1.8f;
		float lastKey = 0;

	public:
		GLuint width, height;
		Camera camera;

		Window();

		Window(GLuint width, GLuint height);

		~Window();


		void update();

		bool shoudClose() const;

		// Inputs
		void process_inputs();

		void size_changed(int width, int height);

		void mouse(double xpos, double ypos);

		void scroll(double xoffset, double yoffset);

		/**
		 * Time diffrence to the last Frame if
		 * Dt is enabled
		 * @return
		 */
		float getDt();

		/**
		 * Time diffrence to the last Frame independent of dt flag
		 * @return
		 */
		float getDtUnpausable();


		GLFWwindow *getWindowPointer() {
			return window;
		}
		// test

	};

}

#endif //OPENGL_QUICKSTART_WINDOW_H
