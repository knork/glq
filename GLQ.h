//
// Created by knork on 12/17/17.
//

#ifndef OPENGL_QUICKSTART_GLQ_H
#define OPENGL_QUICKSTART_GLQ_H


#include <glm/glm.hpp>

#include "src/extern/glad.h"
#include "src/extern/tinyobject.h"
#include "src/window.h"
#include "src/shader.h"
#include "src/camera.h"
#include "src/filesystem.h"
#include "src/drawable/Drawable.h"
#include "src/drawable/mesh.h"
#include "src/drawable/model.h"
#include "src/drawable/geometry.h"
#include "src/drawable/geometryModel.h"
#include "src/Light.h"
#endif //OPENGL_QUICKSTART_GLQ_H
