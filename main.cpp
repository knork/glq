//
// Created by knork on 12/5/17.
//

// test lib

#include <iostream>
#include "GLQ.h"


int main(){
	GLq::Window win;
	glfwSetInputMode(win.getWindowPointer(), GLFW_CURSOR,GLFW_CURSOR_NORMAL);
	// Model not yet working :(
	RenderModel rock("a/rock.obj");
	GeometryModel r2("cube.obj");
	r2.createRenderableMesh();
//	Sphere s(glm::vec3(0,0,0),5);
	Shader shader("base.vs","base.fs");

	glEnable(GL_DEPTH_TEST);

	while (!win.shoudClose()){
		glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		win.process_inputs();
		shader.use();
		glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)win.width / (float)win.height, 0.1f, 1000.0f);
		glm::mat4 view = win.camera.GetViewMatrix();;

		shader.setMat4("projection", projection);
		shader.setMat4("view", view);
		glm::mat4 model;
		model = glm::scale(model,glm::vec3(4,4,2));
		shader.setMat4("model",model);
		r2.draw(shader);
//		s.render(shader);
		win.update();
	}
}